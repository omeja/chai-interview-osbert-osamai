﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="testchai._Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        <asp:Label ID="Label1" runat="server" ForeColor="#000099" 
            style="font-weight: 700" Text="Question 1 - Convert Numbers to Words"></asp:Label>
    </p>
    <p>
        <asp:Label ID="Label3" runat="server" 
            style="font-weight: 700; font-family: Arial; font-size: medium" 
            Text="Type a Number between 0 and 2500"></asp:Label>
        <asp:TextBox ID="txtnum" runat="server" Width="179px"></asp:TextBox>
    </p>
    <p>
        <asp:Button ID="Button3" runat="server" Text="Click to Convert to Words" 
            onclick="Button3_Click" />
    </p>
    <p>
        <asp:Label ID="Label4" runat="server"></asp:Label>
    </p>
    <p>
        &nbsp;</p>
    <p>
        &nbsp;</p>
    <p>
        <asp:Label ID="Label2" runat="server" ForeColor="#000099" 
            style="font-weight: 700" Text="Question 2 - Web Application"></asp:Label>
    </p>
    <p>
        <asp:Button ID="Button1" runat="server" onclick="Button1_Click" 
            Text="Process Excel Files" Width="209px" style="font-weight: 700" />
    </p>
    <p>
        &nbsp;</p>
    <p>
        <asp:HyperLink ID="HyperLink1" runat="server" 
            NavigateUrl="~/frmSalesReport.aspx">View Sales Report By Product</asp:HyperLink>
    </p>
    <p>
        <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/frmRptbyType.aspx">View Sales Report By Product Type</asp:HyperLink>
    </p>
    <p>
        <asp:HyperLink ID="HyperLink3" runat="server" 
            NavigateUrl="~/frmRptByMonth.aspx">View Sales Report By Month</asp:HyperLink>
    </p>
    <p>
        &nbsp;</p>
</asp:Content>
