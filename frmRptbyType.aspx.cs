﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace testchai
{
    public partial class frmRptbyType : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string constring = System.Configuration.ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
            SqlConnection con = new SqlConnection(constring);

            SqlCommand command = new SqlCommand("spgettype", con);
            command.CommandType = CommandType.StoredProcedure;

            con.Open();
            //  int rows = command.ExecuteNonQuery();
            SqlDataReader rdr = null;
            rdr = command.ExecuteReader();

            // Fill the list box with the values retrieved

            while (rdr.Read())
            {
             DropDownList1.Items.Add(rdr[0].ToString());
            }

            con.Close();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string constring = System.Configuration.ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
            SqlConnection con = new SqlConnection(constring);
            String val = DropDownList1.Text;
            SqlCommand command = new SqlCommand("spgetsalesbyproducttype", con);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add("@category", SqlDbType.VarChar).Value = val;
            con.Open();

            SqlDataReader rdr = null;
            DataTable dt = new DataTable();
            rdr = command.ExecuteReader();

            // populate gridview



            dt.Load(rdr);
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
    }
}