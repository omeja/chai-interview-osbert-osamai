﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Office.Interop.Excel;
using Excel = Microsoft.Office.Interop.Excel;
using System.ComponentModel; 
//using Excel=Microsoft.Office.Interop.Excel; 
using System.Reflection;
using System.Data.SqlClient;
using System.Data;
namespace testchai
{
    public partial class _Default : System.Web.UI.Page
    {
        private string typ;
        private string yr;

       
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            // File paths

            String[] paths = new String[] { "D:\\chai\\MonthlySalesReport_MAY.xlsx", "D:\\chai\\MonthlySalesReport_JUNE.xlsx" };

            int cnt = paths.Length;


            for (int x = 0; x < cnt; x++)
            {

                //
                Excel.Application xlApp = new Excel.Application();
                Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(paths[x]);//parameterize file paths and loop
                Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
                Excel.Range xlRange = xlWorksheet.UsedRange;

                int rowCount = xlRange.Rows.Count;
                int colCount = xlRange.Columns.Count;

                //  String [] flds = new  String []{};

                List<string> flds = new List<string>();

                for (int i = 1; i <= rowCount; i++)
                {
                    if ((i >= 8 && i <= 18) || (i >= 20 && i <= 25) || (i >= 27 && i <= 28) || (i >= 30 && i <= 36) || (i >= 30 && i <= 36) || (i >= 38 && i <= 43) || (i >= 45 && i <= 51))
                    {


                        for (int j = 1; j <= colCount; j++)
                        {
                            string val = xlRange.Cells[i, j].Value2.ToString();

                            flds.Add(val);

                            //





                        }

                        if (x == 0)
                        {

                            yr = "May-14";
                        }
                        else {
                            yr = "Jun-14";
                        
                        }


                        if (i >= 8 && i <= 18)
                        {
                            typ = "Beverages";
                        }
                        if (i >= 20 && i <= 25)
                        {

                            typ = "Condiments";
                        }
                        if (i >= 27 && i <= 28)
                        {

                            typ = "Confection";
                        }
                        if (i >= 30 && i <= 36)
                        {

                            typ = "Grains/Cereals";
                        }
                        if (i >= 38 && i <= 43)
                        {

                            typ = "Meat/Poultry";
                        }
                        if (i >= 45 && i <= 51)
                        {
                            typ = "Seafood";

                        }


                        String itm = flds[0];
                        String des = flds[1];
                        String sold = flds[2];
                        String bprice = flds[3];
                        String sprice = flds[4];
                        String categ = typ;
                        //
                        

                        // write to database called chai and a table called sales



                        string constring = System.Configuration.ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
                        SqlConnection con = new SqlConnection(constring);

                        SqlCommand command = new SqlCommand("spaddsales", con);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add("@item_code", SqlDbType.VarChar).Value = itm;
                        command.Parameters.Add("@description", SqlDbType.VarChar).Value = des;
                        command.Parameters.Add("@units_sold", SqlDbType.Decimal).Value = sold;
                        command.Parameters.Add("@buying_price", SqlDbType.Decimal).Value = bprice;
                        command.Parameters.Add("@selling_price", SqlDbType.Decimal).Value = sprice;
                        command.Parameters.Add("@category", SqlDbType.VarChar).Value = categ;
                        command.Parameters.Add("@period", SqlDbType.VarChar).Value = yr;
                        

                        con.Open();
                        int rows = command.ExecuteNonQuery();
                        con.Close();













                        //

                        flds.Clear();

                    }
                    //end if 1






                }
            }
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            string[] Ones = { "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Ninteen" };

            string[] Tens = { "Ten", "Twenty", "Thirty", "Fourty", "Fift", "Sixty", "Seventy", "Eighty", "Ninty" };

            int no = int.Parse(txtnum.Text);
            string words = "";

            if (no > 999 && no < 2501)
            {
                int i = no / 1000;
                words = words + Ones[i - 1] + " Thousand ";
                no = no % 1000;
            }

            if (no > 99 && no < 1000)
            {
                int i = no / 100;
                words = words + Ones[i - 1] + " Hundred ";
                no = no % 100;
            }

            if (no > 19 && no < 100)
            {
                int i = no / 10;
                words = words + Tens[i - 1] + " ";
                no = no % 10;
            }

            if (no > 0 && no < 20)
            {
                words = words + Ones[no - 1];
            }

            if (no <0 || no > 2500)
            {
                words = "Number Entered is out of range";
            }

         
            Label4.Text = words;
        } 
    }
}
