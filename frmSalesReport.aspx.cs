﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace testchai
{
    public partial class frmSalesReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string constring = System.Configuration.ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
            SqlConnection con = new SqlConnection(constring);

            SqlCommand command = new SqlCommand("spgetproduct", con);
            command.CommandType = CommandType.StoredProcedure;
           
            con.Open();
          //  int rows = command.ExecuteNonQuery();
            SqlDataReader rdr = null;
            rdr = command.ExecuteReader();

            // Fill the list box with the values retrieved
           
            while (rdr.Read())
            {
                cboproduct.Items.Add(rdr[0].ToString() );
            }

            con.Close();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string constring = System.Configuration.ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
            SqlConnection con = new SqlConnection(constring);
            String val = cboproduct.Text;
            SqlCommand command = new SqlCommand("spgetsalesbyproduct", con);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add("@description", SqlDbType.VarChar).Value = val;
            con.Open();

            SqlDataReader rdr = null;
            DataTable dt = new DataTable(); 
            rdr = command.ExecuteReader();

            // populate gridview



            dt.Load(rdr);
            this.GridView1.DataSource = dt;
            this.GridView1.DataBind();
        }
    }
}